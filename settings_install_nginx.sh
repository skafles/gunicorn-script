!#/bin/bash

APP_NAME='YoutubeStats' #e.g.YoutubeStats
DIR='/home/ubuntu/youtube'
project_name='youtubestats'
USER='ubuntu' #e.g. ubuntu
GROUP='ubuntu' #e.g.ubuntu 
DJANGO_DIR=$DIR'/'$project_name #e.g....+youtubestats
SITE_URL='ec2-54-245-63-83.us-west-2.compute.amazonaws.com' 

destdir=gunicorn_start

echo -e "#!/bin/bash\nAPP_NAME='$APP_NAME'\nDIR='$DIR'\nproject_name='$project_name'\nUSER='$USER'\nGROUP='$GROUP'\nDJANGO_DIR='$DJANGO_DIR'" > "$destdir"
cat "start_guincorn_1" >> "$destdir"
sudo chmod u+x "$destdir"
cp "$destdir" "$DIR"/bin/ 

sudo apt-get install nginx
sudo service nginx start

sed -e "s|django_folder_location|$DIR|g;;s|django_complete_location|$DIR/$project_name|g;s|example_com|$SITE_URL|g" sites-available1 > "$APP_NAME"
sudo cp "$APP_NAME" /etc/nginx/sites-available/
sudo cp "$APP_NAME" /etc/nginx/sites-enabled/

sudo ln -s /etc/nginx/sites-available/"$APP_NAME" /etc/nginx/sites-enabled/"$APP_NAME"

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python-virtualenv
virtualenv $DIR
cd $DIR
source $DIR/bin/activate
pip install django
pip install gunicorn
pip install setproctitle

# gunicorn $APP_NAME.wsgi:application
sudo apt-get install python-dev


sudo apt-get install supervisor

confdir=/etc/supervisor/conf.d/
# confdir=$APP_NAME.conf
echo -e "[program:$APP_NAME]\ncommand = $DIR/bin/$destdir\nuser = $USER\nstdout_logfile = $DIR/logs/gunicorn_supervisor.log\nredirect_stderr = true " > "$APP_NAME".conf
sudo mv $APP_NAME.conf $confdir
sudo supervisorctl update

sudo mkdir -p $DIR/logs/
sudo touch $DIR/logs/gunicorn_supervisor.log 

sudo service nginx restart
